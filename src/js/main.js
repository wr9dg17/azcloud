$(document).ready(function() {
    // Intro slider
    const introSlider = new Swiper(".intro-slider .swiper-container", {
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true
        },
        autoplay: {
            delay: 7000
        },
        on: {
            slideChange: () => {
                const currentIndex = introSlider.activeIndex + 1;
                $(".fraction .current").text(currentIndex);
            }
        },
        effect: "fade",
        fadeEffect: {
            crossFade: true
        },
        breakpoints: {
            1024: {
                noSwipingClass: "no-swipe",
            }
        }
    });

    // Advantages slider
    const advantagesSlider = new Swiper(".advantages .swiper-container", {
        slidesPerView: "auto",
        spaceBetween: 30,
        freeModeMomentumRatio: 0.6,
        freeModeMomentumVelocityRatio: 0.6,
        breakpoints: {
            1024: {
                freeMode: true,
            }
        }
    });

    // Feedback slider
    const feedbackThumbs = new Swiper(".feedback-thumbs .swiper-container", {
        slidesPerView: "auto",
        spaceBetween: 24,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            768: {
                spaceBetween: 32,
            }
        }
    });

    const feedbackSlider = new Swiper(".feedback-top .swiper-container", {
        spaceBetween: 100,
        pagination: {
            el: '.swiper-pagination',
            type: 'progressbar',
        },
        thumbs: {
            swiper: feedbackThumbs
        }
    });

    // ********
    $(".to-services").click(() => {
        $("html, body").animate({
            scrollTop: $(".services-body").offset().top
        }, 400);
    });

});