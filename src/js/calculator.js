$(document).ready(function () {
    // -- change numeric field value
    let closestInput;
    let closestInputVal;
    const decrementBtn = $(".decrement");
    const incrementBtn = $(".increment");
    const counterInput = $(".number-input");

    incrementBtn.on("click", function () {
        closestInput = $(this).closest(".field-item").find("input");
        closestInputVal = Number(closestInput.val());
        closestInput.val(closestInputVal + 1);
        closestInput.trigger("change");
    });

    decrementBtn.on("click", function () {
        closestInput = $(this).closest(".field-item").find("input");
        closestInputVal = Number(closestInput.val());
        if (closestInputVal === 0) return;
        closestInput.val(closestInputVal - 1);
        closestInput.trigger("change");
    });

    counterInput.on("change", function () {
        const val = Number($(this).val());
        if (val < 0) $(this).val("1");
    });

    // -- init calculator
    initCalculator();

    // -- filter virtual machines
    const osField = $("[name='os']");
    const osFamilyField = $("[name='os-family']");

    osField.on("change", function () {
        const os = $(this).val();
        const osFamily = $("[name='os-family']:checked").val();
        filterVMachines(os, osFamily);
        calculateTotal();
    });

    osFamilyField.on("change", function () {
        const osFamily = $(this).val();
        const os = $("[name='os']:checked").val();
        filterVMachines(os, osFamily);
        calculateTotal();
    });

    // -- filter storage families
    const discFamilyField = $("[name='disc-family']");

    discFamilyField.on("change", function () {
        const discFamily = $(this).val();
        filterStorages(discFamily);
        calculateTotal();
    });

    // calculate VM cost on fields change
    const vmField = $("[name='vm']");
    const vmQtyField = $("[name='vm-qty']");
    const vmDurationField = $("[name='vm-duration']");
    const vmDurationType = $("[name='vm-duration-type']");
    // calculate storage cost on fields change
    const discSizeField = $("[name='disc-size']");
    const discQtyField = $("[name='disc-qty']");
    const discDurationField = $("[name='disc-duration']");
    const discDurationType = $("[name='disc-duration-type']");

    [
        vmField,
        vmQtyField,
        vmDurationField,
        vmDurationType,
        discSizeField,
        discQtyField,
        discDurationField,
        discDurationType,
    ].forEach((field) => field.on("change", calculateTotal));
});

// helpers
function initCalculator() {
    const os = $("[name='os']:checked").val();
    const osFamily = $("[name='os-family']:checked").val();
    const discFamily = $("[name='disc-family']:checked").val();
    filterVMachines(os, osFamily);
    filterStorages(discFamily);
    calculateTotal();
}

function filterVMachines(os, osFamily) {
    $("option[data-os]").hide();
    $(`option[data-family=${os}-${osFamily}]`).show();
    $("select[name='vm']").val($(`[data-family=${os}-${osFamily}]`).val());
}

function filterStorages(family) {
    $("option[data-disc-family]").hide();
    $(`option[data-disc-family=${family}]`).show();
    $("select[name='disc-size']").val($(`[data-disc-family=${family}]`).val());
}

function calculateHoursIn(period) {
    switch (period) {
        case "weeks":
            return 168;
        case "months":
            return 720;
        default:
            return 1;
    }
}

function calculateTotal() {
    // VM calculation
    const vm = Number($("[name='vm']").val());
    const vmQty = Number($("[name='vm-qty']").val());
    const vmDuration = Number($("[name='vm-duration']").val());
    const vmDurationType = $("[name='vm-duration-type']").val();
    const vmDurationDelta = calculateHoursIn(vmDurationType);
    const vmSum = (vm * vmQty) * (vmDuration * vmDurationDelta);

    // Storage calculation
    const discFamily = Number($("[name='disc-size']").val());
    const discQty = Number($("[name='disc-qty']").val());
    const discDuration = Number($("[name='disc-duration']").val());
    const discDurationType = $("[name='disc-duration-type']").val();
    const discDurationDelta = calculateHoursIn(discDurationType);
    const discSum = (discFamily * discQty) * (discDuration * discDurationDelta);

    $(".price span").text((vmSum + discSum).toFixed(2));
}
