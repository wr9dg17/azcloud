$(document).ready(function() {
    // -- sticky share icons init
    $(".sticky").stick_in_parent({
        offset_top: 24
    });

    // -- blog gallery
    const galleryThumbs = new Swiper(".gallery-thumbs .swiper-container", {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        slidesPerView: "auto",
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            480: {
                spaceBetween: 22,
            },
            300: {
                spaceBetween: 8
            }
        }
    });

    const galleryTop = new Swiper(".gallery-top .swiper-container", {
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        spaceBetween: 30,
        thumbs: {
            swiper: galleryThumbs
        }
    });

    // -- copy to clipboard
    $(".action-copy").on("click", function (e) {
        e.preventDefault();
        const $input = $(this).closest("li").find("input");
        const successText = $(this).data("copied");
        $input.select();
        document.execCommand("copy");
        $(this).text(successText);
    });

});