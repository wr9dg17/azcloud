$(document).ready(function() {
    // -- sticky total price init
    $(".sticky").stick_in_parent({
        offset_top: 24
    });

    $(".sticky-nav").stick_in_parent();

    $("body").scrollspy({
        target: "#service-nav",
        offset: $(".service-nav").height() * 1.7
    })

    // -- toggle service description
    const serviceDescription = $(".service-description");
    const serviceRestChunk = $(".rest-chunk");
    const expandBtn = $(".expand");
    const rollUpBtn = $(".roll-up");

    expandBtn.on("click", () => {
        serviceDescription.addClass("expanded");
        serviceRestChunk.slideDown(200);
    });

    rollUpBtn.on("click", () => {
        serviceDescription.removeClass("expanded");
        serviceRestChunk.slideUp(200);
    });

    // -- smooth navigation
    $(".service-nav .nav-link").click(function() {
        var target = $(this).attr("href");

        $("html, body").animate({
            scrollTop: $(target).offset().top - $(".service-nav").height()
        }, 400);
    });

    // -- use cases hover effect
    $(".use-case__example").on("mouseenter", function() {
        const img = $(this).data("src");
        $(".use-cases__img img").attr("src", img);
    });

    // -- features slider
    const featuresSlider = new Swiper(".features-slider .swiper-container", {
        autoHeight: true,
        lazy: {
            loadPrevNext: true,
            loadPrevNextAmount: 2,
            preloadImages: false
        },
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });

    // -- filter prices
    const osField = $("[name='service-os']");
    const osFamilyField = $("[name='service-os-family']");

    initialFilter();

    osField.on("change", function () {
        const os = $(this).val();
        const osFamily = $("[name='service-os-family']:checked").val();
        filterPrices(os, osFamily);
    });

    osFamilyField.on("change", function () {
        const osFamily = $(this).val();
        const os = $("[name='service-os']:checked").val();
        filterPrices(os, osFamily);
    });

    // -- collapse all accordions
    $(".collapse-all").on("click", function() {
        $(this)
            .closest(".bundle-params")
            .find(".collapse")
            .collapse("hide");
    });

});

function initialFilter() {
    const os = $("[name='service-os']:checked").val();
    const osFamily = $("[name='service-os-family']:checked").val();
    filterPrices(os, osFamily);
}

function filterPrices(os, osFamily) {
    $("tr[data-os]").hide();
    $(`tr[data-family=${os}-${osFamily}]`).show();
}
