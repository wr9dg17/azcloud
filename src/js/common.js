// Styles
import "../css/fontello.css";
import "../css/swiper.min.css";
import "aos/dist/aos.css";
import "../css/nice-select.css";
import "../scss/style.scss";
// JS
import AOS from "aos";
import "./bootstrap/bootstrap";

$(document).ready(function() {
    // AOS init
    AOS.init({
        once: true,
        disable: "mobile"
    });

    // Media lazy load init
    yall({ observeChanges: false });

    // Tooltips init
    $('[data-toggle="tooltip"]').tooltip();

    // Header
    const header = $("header");
    $(window).on("scroll", () => {
        if ($(window).scrollTop() > 50) {
            header.addClass("scrolling");
        } else {
            header.removeClass("scrolling");
        }
    });

    // Toggle menu
    $(".menu-toggler").on("click", () => {
        $("html").toggleClass("menu-visible");
    });

    // Menu cards animation
    const menuLink = $(".menu-link");
    const servicesLink = $(".services-link");
    const menuRightContainer = $(".menu-right-container");

    menuLink.on("mouseenter", () => {
        menuRightContainer.removeClass("hovered");
        menuRightContainer.addClass("default");
    });

    servicesLink.on("mouseenter", () => {
        menuRightContainer.removeClass("default");
        menuRightContainer.addClass("hovered");
    });
    
    // Toggle search
    $(".search-toggler").on("click", () => {
        window.scrollTo(0, 0);
        $("html").toggleClass("search-visible");
    });

    // Clients / partners slider options
    const participansSliderOptions = {
        centeredSlides: true,
        autoplay: {
            delay: 5000
        },
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true
        },
        breakpoints: {
            768: {
                slidesPerView: 3
            },
            575: {
                slidesPerView: 2,
                centeredSlides: false
            },
            300: {
                slidesPerView: 1,
                centeredSlides: false
            }
        }
    }

    const clientsSliderContainer = document.querySelector(".clients-slider .swiper-container");
    const partnersSliderContainer = document.querySelector(".partners-slider .swiper-container");

    if (clientsSliderContainer) {
        const clientsSlider = new Swiper(clientsSliderContainer, participansSliderOptions);
    }

    if (partnersSliderContainer) {
        const partnersSlider = new Swiper(partnersSliderContainer, participansSliderOptions);
    }

    // Partnership page
    const advantagesSliderContainer = document.querySelector(".partnership-advantages .swiper-container");

    if (advantagesSliderContainer) {
        const advantagesSlider = new Swiper(".partnership-advantages .swiper-container", {
            breakpoints: {
                1610: {
                    slidesPerView: 2.4,
                    spaceBetween: 80
                },
                1200: {
                    slidesPerView: 2,
                    spaceBetween: 56
                },
                900: {
                    slidesPerView: 2.4
                },
                575: {
                    slidesPerView: 1.5,
                    spaceBetween: 40
                },
                300: {
                    slidesPerView: 1.1,
                    spaceBetween: 24
                }
            }
        });

        $(".partnership-advantages .swiper-button-prev").click(() => advantagesSlider.slidePrev());
        $(".partnership-advantages .swiper-button-next").click(() => advantagesSlider.slideNext());
    }

    // -- universal block gallery
    const universalBlock = $(".universal-block");

    if (universalBlock.length > 0) {
        const blockGallery = new Swiper(".block-gallery .swiper-container", {
            lazy: {
                loadPrevNext: true,
                loadPrevNextAmount: 2,
                preloadImages: false
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: ".swiper-pagination",
                type: "bullets",
                clickable: true
            }
        });
    }

    // -- to top button
    let offset = 500,
        scroll_top_duration = 400,
        beforeScroll = $(window).scrollTop(),
        $toTop = $(".to-top");

    $toTop.on("click", () => {
        $("body, html").animate({scrollTop: 0}, scroll_top_duration);
    });

    $(window).scroll(function () {
        const scrollTop = $(this).scrollTop();

        if (scrollTop > offset) {
            if (scrollTop < beforeScroll && !$toTop.hasClass("active")) {
                $toTop.addClass("active");
            }
        } else if ($toTop.hasClass("active")) {
            $toTop.removeClass("active");
        }
        
        beforeScroll = scrollTop;
    });

});
